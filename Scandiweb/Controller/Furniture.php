<?php namespace Scandiweb\Controller;
    require '../../vendor/autoload.php';
    use Scandiweb\Controller\Product;
    use Scandiweb\Model\ProductDAO;
    class Furniture extends Product{
        
        private $height;
        private $width;
        private $length;

        function getHeight() {
            return $this->height;
        }

        function getWidth() {
            return $this->width;
        }

        function getLength() {
            return $this->length;
        }

        function setHeight($height) {
            $this->height = $height;
        }

        function setWidth($width) {
            $this->width = $width;
        }

        function setLength($length) {
            $this->length = $length;
        }
    }
    ob_start();
    $furniture = new Furniture();
    $furniture->setSku($_POST['sku']);
    $furniture->setName($_POST['name']);
    $furniture->setPrice($_POST['price']);
    $furniture->setWidth($_POST['width']);
    $furniture->setHeight($_POST['height']);
    $furniture->setLength($_POST['length']);
    $productDao = new ProductDAO();
    $productDao->insertFurniture($furniture);
    header("Location:../../index.php");
    ob_end_flush();
?>



