<?php namespace Scandiweb\Controller;
    require '../../vendor/autoload.php';
    use Scandiweb\Controller\Product;
    use Scandiweb\Model\ProductDAO;
    class Book extends Product{
        
        private $weight;

        function getWeight() {
            return $this->weight;
        }

        function setWeight($weight) {
            $this->weight = $weight;
        }
    }
    ob_start();
    $book = new Book();
    $book->setSku($_POST['sku']);
    $book->setName($_POST['name']);
    $book->setPrice($_POST['price']);
    $book->setWeight($_POST['weight']);
    $productDao = new ProductDAO();
    $productDao->insertBook($book);
    header("Location:../../index.php");
    ob_end_flush();
?>


