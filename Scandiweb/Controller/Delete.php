<?php namespace Scandiweb\Controller;
    require '../../vendor/autoload.php';
    use Scandiweb\Model\ProductDAO;
    class Delete {
        function delete($id){
            $productDao = new ProductDAO();
            $productDao->massDelete($id);
        }
    }
    ob_start();
    use Scandiweb\Controller\Delete;
    $delete = new Delete();
    $delete->Delete($_POST['deleteId']);
    header("Location:../../index.php");
    ob_end_flush();

