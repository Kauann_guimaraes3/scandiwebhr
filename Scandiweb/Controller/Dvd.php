<?php namespace Scandiweb\Controller;
    require '../../vendor/autoload.php';
    use Scandiweb\Controller\Product;
    use Scandiweb\Model\ProductDAO;
    class Dvd extends Product{
        
        private $size;

        function getSize() {
            return $this->size;
        }

        function setSize($size) {
            $this->size = $size;
        }
    }
    ob_start();
    $dvd = new Dvd();
    $dvd->setSku($_POST['sku']);
    $dvd->setName($_POST['name']);
    $dvd->setPrice($_POST['price']);
    $dvd->setSize($_POST['size']);
    $productDao = new ProductDAO();
    $productDao->insertDvd($dvd);
    header("Location:../../index.php");
    ob_end_flush();
?>

