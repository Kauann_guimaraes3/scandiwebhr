<?php namespace Scandiweb\Controller;
    abstract class Product {
        private $sku;
        private $name;
        private $price;
        private $type;
                
        function getSku() {
            return $this->sku;
        }

        function getName() {
            return $this->name;
        }

        function getPrice() {
            return $this->price;
        }

        function setSku($sku) {
            $this->sku = $sku;
        }

        function setName($name) {
            $this->name = $name;
        }

        function setPrice($price) {
            $this->price = $price;
        }
        function getType() {
            return $this->type;
        }

        function setType($type, $data) {
            $this->type = $type;
        }
}

?>