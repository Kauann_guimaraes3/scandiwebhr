<?php namespace Scandiweb\Controller;
use Scandiweb\Model\ProductDAO;
class DisplayProducts {
        function formatPrint($result){
            $sku = $result['sku'];
            echo "<input class='delete-checkbox' type='checkbox' name='deleteId[]' value='" .$sku."'>";
            echo "<p class='h5 text-center'>" . $sku . "</p>";
            echo "<p class='h5 text-center'>" . $result['product_name'] . "</p>";
            echo "<p class='h5 text-center'>" . $result['product_price']. " $</p>";
            echo "<p class='h5 text-center'>" . (isset($result['size']) ? $result['size'] . "MB</p><br><br><br>" : "");
            echo "<p class='h5 text-center'>" . (isset($result['weight']) ? $result['weight'] . "KG</p><br><br><br>" : "");
            echo "<p class='h5 text-center'>" . (isset($result['length']) ? $result['length'] . " X " : "");
            echo (isset($result['height']) ? $result['height'] . " X " : "");
            echo (isset($result['width']) ? $result['width'] . "</p><br><br><br>" : "");
        }
    }
?>

