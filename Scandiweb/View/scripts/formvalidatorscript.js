$(document).ready(function() {
  $('#product_form').validate({
    rules: {
      sku:{
          required:true
      },
      name: {
          required:true
      },
      price:{
          required:true,
          number:true
      },
      productType:{
          required:true
      },
      size:{
        required:true,
          number:true
      },
      height:{
         required:true,
          number:true 
      },
      width:{
          required:true,
          number:true
      },
      lenght:{
          required:true,
          number:true
      },
      weight:{
          required:true,
          number:true
      },
    },
    messages: {
      sku: {
        required: "<nobr><small>Please, submit required data</small></nobr>"
      },
      name: {
        required: "<nobr><small>Please, submit required data</small></nobr>"
      },
      price: {
        required: "<nobr><small>Please, submit required data</small></nobr>",
        number: "<nobr><small>Please, provide the data of indicated type</nobr></small>"
      },
      productType:{
         required: "<nobr><small>Please, submit required data</small></nobr>"
      },
      size:{
        required: "<nobr><small>Please, submit required data</small></nobr>",
        number: "<nobr><small>Please, provide the data of indicated type</nobr></small>"
      },
      height:{
          required: "<nobr><small>Please, submit required data</small></nobr>",
          number: "<nobr><small>Please, provide the data of indicated type</nobr></small>"
      },
      width:{
          required: "<nobr><small>Please, submit required data</small></nobr>",
          number: "<nobr><small>Please, provide the data of indicated type</nobr></small>"
      },
      lenght:{
          required: "<nobr><small>Please, submit required data</small></nobr>",
          number: "<nobr><small>Please, provide the data of indicated type</nobr></small>"
      },
      weight:{
          required: "<nobr><small>Please, submit required data</small></nobr>",
          number: "<nobr><small>Please, provide the data of indicated type</nobr></small>"
      },
    },
    submitHandler: function(form) {
      form.submit();
    }
  });
});

