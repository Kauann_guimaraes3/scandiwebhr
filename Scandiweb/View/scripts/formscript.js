$(document).ready(function() {
    $("#productType").change(function() {
        var selected = $(this).children(":selected").text();
        switch (selected) {
            case "DVD":
                var $formAdd = $("<p>Size (MB)<input type='text' id='size' name='size'></p><p><strong>Please, provide disc space in MB</strong></p>");
                $('#formType').html($formAdd);
                $('#product_form').attr('action', 'Scandiweb/Controller/Dvd.php');
                break;
            case "Furniture":
                var $formAdd = $("<p>Height (CM) <input type='text' id='height' name='height'></p><p>Width (CM) <input type='text' id='width' name='width'></p><p>Length (CM) <input type='text' id='length' name='length'></p><p><strong>Please, provide height, width and length</strong></p>");
                $('#formType').html($formAdd);
                $('#product_form').attr('action', 'Scandiweb/Controller/Furniture.php');
                break;
            case "Book":
                var $formAdd = $("<p>Weight (KG)<input type='text' id='weight' name='weight'></p><p><strong>Please, provide weight in kilograms</strong></p>");
                $('#formType').html($formAdd);
                $('#product_form').attr('action', 'Scandiweb/Controller/Book.php');
                break;
            default:
                var $formAdd = $("");
                $('#formType').html($formAdd);
        }
    });
});


