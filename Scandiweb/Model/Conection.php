<?php 
    namespace Scandiweb\Model;
    use PDO;
    abstract class Conection{
        public static function getInstance(){
            $servername = "localhost";
            $username = "";
            $password = "";
            $database = "";
            try {
                $pdo = new PDO("mysql:host=$servername;dbname=$database", $username, $password);
                $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
                return $pdo;
            } catch(PDOException $e) {    
                echo "Connection failed: " . $e->getMessage();
            }
        }
    }
?>