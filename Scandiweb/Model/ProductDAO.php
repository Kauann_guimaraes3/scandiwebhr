<?php
    namespace Scandiweb\Model;
    use Scandiweb\Model\Conection;
    require 'Conection.php';
    use PDO;
    class ProductDAO extends Conection{
        function insertDvd($product){
            
            try {
                $pdo = Conection::getInstance();
                $sql = ("insert into type(type_name, size) values ('dvd', ?); SET @idatt =  LAST_INSERT_ID(); insert into product (sku, product_name, product_price, type_idtype ) values (?, ?, ?, @idatt)");
                $stmt = $pdo->prepare($sql);
                $stmt->bindValue(1, $product->getSize());
                $stmt->bindValue(2, $product->getSku());
                $stmt->bindValue(3, $product->getName());
                $stmt->bindValue(4, $product->getPrice());
                $stmt->execute();
            } catch (PDOException $ex) {
                echo $ex;
            }
        }
        function insertBook($product){
            try {
                $pdo = Conection::getInstance();
                $sql = ("insert into type(type_name, weight) values ('book', ?); SET @idatt = LAST_INSERT_ID(); insert into product (sku, product_name, product_price, type_idtype ) values (?, ?, ?, @idatt)");
                $stmt = $pdo->prepare($sql);
                $stmt->bindValue(1, $product->getWeight());
                $stmt->bindValue(2, $product->getSku());
                $stmt->bindValue(3, $product->getName());
                $stmt->bindValue(4, $product->getPrice());
                $stmt->execute();
            } catch (PDOException $ex) {
                echo $ex;
            }
        }
        function insertFurniture($product){
            try {
                $pdo = Conection::getInstance();
                $sql = ("insert into type(type_name, width, height, length) values ('furniture', ?, ?, ?); SET @idatt = last_insert_id(); insert into product (sku, product_name, product_price, type_idtype ) values (?, ?, ?, @idatt)");
                $stmt = $pdo->prepare($sql);
                $stmt->bindValue(1, $product->getWidth());
                $stmt->bindValue(2, $product->getHeight());
                $stmt->bindValue(3, $product->getLength());
                $stmt->bindValue(4, $product->getSku());
                $stmt->bindValue(5, $product->getName());
                $stmt->bindValue(6, $product->getPrice());
                $stmt->execute();
            } catch (Exception $ex) {
                 echo $ex;
            }
        }
        function massDelete($id){
            try {
                
                $pdo = Conection::getInstance();
                foreach ($id as $id){
                $sql = ("DELETE pro.*, proatt FROM product pro INNER JOIN type proatt ON pro.type_idtype = proatt.idtype WHERE sku in (?)");
                $stmt= $pdo->prepare($sql);
                $stmt->bindValue(1, $id);
                $stmt-> execute();
                }
            } catch (PDOException $ex) {
                echo $ex;
            }
        }
        function selectAll(){
            try {
                $pdo = Conection::getInstance();
                $sql = ("select sku, product_name, product_price, weight, size, length, width, height, type_name from product inner join type on type_idtype = idtype");
                $stmt= $pdo->prepare($sql);
                $stmt-> execute();
                $result = $stmt->fetchAll();
                return $result;
            } catch (PDOException $ex) {
                echo $ex;
            }
        }
    }
?>
