<?php require 'vendor/autoload.php'; 
    use Scandiweb\Controller\DisplayProducts;
    use Scandiweb\Model\ProductDAO;
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <link rel="stylesheet" href="Scandiweb/View/bootstrap-4.6.0-dist/bootstrap-4.6.0-dist/css/bootstrap.css">
        <title></title>
    </head>
    <body>
        <nav class="navbar navbar-light justify-content-between">
            <a class="navbar-brand">Product list</a>
            <div>
                <a class="btn btn-primary" href="add-product.php">ADD</a>
                <button class="btn btn-danger" type="submit" form="deleteForm" id="delete-product-btn">MASS DELETE</button>   
            </div>
        </nav>
        <hr>
        <div class="container">
            <form id="deleteForm" action="Scandiweb/Controller/Delete.php" method="POST">
                <div class='row top'>    
                    <?php 
                    $formatProduct = new DisplayProducts();
                    $productDao = new ProductDAO();
                    $result = $productDao->selectAll();
                    foreach ($result as $result){       
                        echo "<div class='col-3 top'>";
                        echo "<div class='card'>";
                        $formatProduct->formatPrint($result);
                        echo "</div></div>";
                    }
                    ?>
                </div>
            </form>
        </div>
        <br>
        <hr>
        <p class="text-center">Scandiweb Test assignment</p>
    </body>
</html>
