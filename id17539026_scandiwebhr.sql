-- phpMyAdmin SQL Dump
-- version 4.9.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Tempo de geração: 06-Set-2021 às 19:27
-- Versão do servidor: 10.3.18-MariaDB
-- versão do PHP: 7.3.23

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Banco de dados: `id17539026_scandiwebhr`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `product`
--

CREATE TABLE `product` (
  `sku` varchar(45) NOT NULL,
  `product_name` varchar(45) NOT NULL,
  `product_price` double NOT NULL,
  `type_idtype` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `type`
--

CREATE TABLE `type` (
  `idtype` int(11) NOT NULL,
  `type_name` varchar(45) NOT NULL,
  `size` int(11) DEFAULT NULL,
  `weight` double DEFAULT NULL,
  `width` double DEFAULT NULL,
  `height` double DEFAULT NULL,
  `length` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `type`
--

INSERT INTO `type` (`idtype`, `type_name`, `size`, `weight`, `width`, `height`, `length`) VALUES
(1, 'book', NULL, 45, NULL, NULL, NULL),
(2, 'book', NULL, 4, NULL, NULL, NULL),
(3, 'book', NULL, 4, NULL, NULL, NULL),
(7, 'dvd', 32, NULL, NULL, NULL, NULL),
(8, 'dvd', 32, NULL, NULL, NULL, NULL),
(9, 'dvd', 32, NULL, NULL, NULL, NULL),
(23, 'dvd', 32, NULL, NULL, NULL, NULL),
(33, 'book', NULL, 45, NULL, NULL, NULL),
(41, 'dvd', 32, NULL, NULL, NULL, NULL),
(43, 'dvd', 34, NULL, NULL, NULL, NULL);

--
-- Índices para tabelas despejadas
--

--
-- Índices para tabela `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`sku`),
  ADD KEY `fk_product_type_idx` (`type_idtype`);

--
-- Índices para tabela `type`
--
ALTER TABLE `type`
  ADD PRIMARY KEY (`idtype`);

--
-- AUTO_INCREMENT de tabelas despejadas
--

--
-- AUTO_INCREMENT de tabela `type`
--
ALTER TABLE `type`
  MODIFY `idtype` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=51;

--
-- Restrições para despejos de tabelas
--

--
-- Limitadores para a tabela `product`
--
ALTER TABLE `product`
  ADD CONSTRAINT `fk_product_type` FOREIGN KEY (`type_idtype`) REFERENCES `type` (`idtype`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
