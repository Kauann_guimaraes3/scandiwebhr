<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <link rel="stylesheet" href="Scandiweb/View/bootstrap-4.6.0-dist/bootstrap-4.6.0-dist/css/bootstrap.css">
        <script src="Scandiweb/View/jquery/jquery-3.6.0.min.js"></script>
        <script src="Scandiweb/View/jquery/jquery-validation-1.19.3/dist/jquery.validate.js"></script>
        <script src="Scandiweb/View/scripts/formscript.js"></script>
        <script src="Scandiweb/View/scripts/formvalidatorscript.js"></script>
        <title></title>
    </head>
    <body>
        <nav class="navbar navbar-light justify-content-between">
            <a class="navbar-brand">Product add</a>
            <div>
                <button class="btn btn-primary" type="submit" form="product_form">Save</button>
                <a class="btn btn-danger" type="reset" form="product_form" href="index.php">Cancel</a>
            </div>
        </nav>
        <hr>
        <div class="pl-md-3">
            <form action="#" id="product_form" name="product_form" method="POST">
                <p>SKU <input type="text" id="sku" name="sku" ></p>
                <p>Name <input type="text" id="name" name="name" ></p>
                <p>Price ($) <input type="text" id="price" name="price" ></p>
                <p>
                    Type Switcher <select id="productType" name="productType">
                        <option value="">Type Switcher</option>
                        <option value="dvd">DVD</option>
                        <option value="book">Book</option>
                        <option value="furniture">Furniture</option>   
                    </select>
                </p>
                <div id="formType"></div>
            </form>
        </div>
        <hr>
        <p class="text-center">Scandiweb Test assignment</p>
    </body>
</html>

